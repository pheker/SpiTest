package cn.pheker.spi;

import java.lang.annotation.Annotation;
import java.util.*;
import java.util.function.Consumer;

/**
 * @author cn.pheker
 * @date   2019/2/22 23:55
 * @email  1176479642@qq.com
 * @desc   Class工具类
 */
public class ClassUtil {
    
    private static Reflections reflections = new Reflections("cn.pheker");
    
    /**
     * 获取被注解的接口和类
     * @param annotationType
     * @return
     */
    public static Set<Class<?>> getTypesAnnotatedWith(Class<? extends Annotation> annotationType){
        Set<Class<?>> typesAnnotatedWith = reflections.getTypeAnnotatedWith(annotationType);
        return typesAnnotatedWith;
    }
    
    /**
     * 获取被注解的接口或类
     * @param annotationType
     * @param interfaceOrClass true:只保留接口, false:只保留类
     * @return
     */
    public static Set<Class<?>> getTypesAnnotatedWith(Class<? extends Annotation> annotationType, boolean interfaceOrClass){
        Set<Class<?>> typesAnnotatedWith = reflections.getTypeAnnotatedWith(annotationType);
            Iterator<Class<?>> ite = typesAnnotatedWith.iterator();
            while (ite.hasNext()) {
                Class<?> next = ite.next();
                if (interfaceOrClass && !next.isInterface()) {
                    ite.remove();
                } else if (!interfaceOrClass && next.isInterface()){
                    ite.remove();
                }
            }
        return typesAnnotatedWith;
    }
    
    
    /**
     * 调用某个接口(此接口有Spi注解)的实现类
     * @param interfaceClzz
     * @param consumer
     * @return 实现类数量
     */
    public static <T> int invokeSpi(Class<T> interfaceClzz, Consumer<T> consumer)
            throws NoImplementClassException {
        Set<T> impls = getImplementsOfInterfaceWithSpi(interfaceClzz);
        int size = impls.size();
        if (size == 0) {
            throw new NoImplementClassException(interfaceClzz);
        } else{
            Iterator<T> impIte = impls.iterator();
            while (impIte.hasNext()) {
                consumer.accept(impIte.next());
            }
        }
        return size;
    }
    
    /**
     * 调用某个接口(此接口有Spi注解)的实现类
     * @param interfaceClzz
     * @param consumer
     */
    public static <T> void invokeSpi(Class<T> interfaceClzz, boolean permitMulti, Consumer<T> consumer)
            throws NoImplementClassException, MultiImplementClassException {
        int size = invokeSpi(interfaceClzz, consumer);
        //出现多个实现类，但不允许有多个时
        if (size > 1 && !permitMulti) {
            throw new MultiImplementClassException(interfaceClzz);
        }
    }
    
    /**
     * 获取接口的实现类
     * @param interfaceClzz 接口，此接口必须有Spi注解
     * @param <T>
     * @return
     */
    public static <T> Set<T> getImplementsOfInterfaceWithSpi(Class<T> interfaceClzz) {
        Set<T> set = new HashSet<>();
        // 1.获取spi注解的服务接口
        Set<Class<?>> clzzesWithSpi = ClassUtil.getTypesAnnotatedWith(Spi.class, true);
        for (Class clzzWithSpi :clzzesWithSpi) {
            // 2.根据服务接口获取具体的服务实现
            ServiceLoader serviceLoader = ServiceLoader.load(clzzWithSpi);

            // 3.进行服务(如：扫描图形码)
            Iterator<?> serviceIte = serviceLoader.iterator();
            while (serviceIte.hasNext()) {
                set.add((T) serviceIte.next());
            }
        }
        return set;
    }
    
}
