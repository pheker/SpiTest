package cn.pheker.spi.test;

import cn.pheker.spi.Spi;

/**
 * @author cn.pheker
 * @date 2019/2/24 16:20
 * @email 1176479642@qq.com
 * @desc
 */

@Spi
public class ClassOfInterfaceWithSpi implements InterfaceWithSpi {
    @Override
    public void print() {
        System.out.println(this.getClass());
    }
}
