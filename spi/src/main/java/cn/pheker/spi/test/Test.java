package cn.pheker.spi.test;

import cn.pheker.spi.ClassUtil;
import cn.pheker.spi.Spi;

import java.util.Set;

/**
 * @author cn.pheker
 * @date 2019/2/24 16:16
 * @email 1176479642@qq.com
 * @desc
 */
public class Test {
    
    
    public static void main(String[] args) {
        Set<Class<?>> typesAnnotatedWith = ClassUtil.getTypesAnnotatedWith(Spi.class);
        System.out.println("typesAnnotatedWith = " + typesAnnotatedWith);
    
        Set<Class<?>> interfaces = ClassUtil.getTypesAnnotatedWith(Spi.class, true);
        System.out.println("interfaces = " + interfaces);
    
        Set<Class<?>> classes = ClassUtil.getTypesAnnotatedWith(Spi.class, false);
        System.out.println("classes = " + classes);
    }
    
    
}
