package cn.pheker.spi.test;

import cn.pheker.spi.Spi;

/**
 * @author cn.pheker
 * @date 2019/2/24 16:19
 * @email 1176479642@qq.com
 * @desc
 */
@Spi
public interface InterfaceWithSpi {
    
    void print();
    
}
