package cn.pheker.spi;

/**
 * @author cn.pheker
 * @date   2019/2/23 0:59
 * @email  1176479642@qq.com
 * @desc   如果一个接口有多个实现类，将报MultiImplementClassException
 */
public class NoImplementClassException extends Exception {
    
    public NoImplementClassException(Class interfaceClzz) {
        super("Don't found implement class of " + interfaceClzz.getName());
    }
    
}
