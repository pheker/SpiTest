package cn.pheker.spi;

import java.lang.annotation.*;

/**
 * @author cn.pheker
 * @date   2019/2/22 23:01
 * @email  1176479642@qq.com
 * @desc   Declare interface is an SPI interface
 */

@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
@Inherited
public @interface Spi {
}
