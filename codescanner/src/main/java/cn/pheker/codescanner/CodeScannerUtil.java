package cn.pheker.codescanner;

import cn.pheker.spi.ClassUtil;
import cn.pheker.spi.MultiImplementClassException;
import cn.pheker.spi.NoImplementClassException;

import java.util.Set;

/**
 * @author cn.pheker
 * @date   2019/2/22 23:13
 * @email  1176479642@qq.com
 * @desc   图形码扫描器工具类
 */
public class CodeScannerUtil {
    
    /**
     * 扫描器-功能：扫描图形码
     */
    public static void scan(){
        try {
            ClassUtil.invokeSpi(CodeScanner.class, cs ->{
                String info = cs.scan();
                System.out.println(String.format("[CodeScanner]: %s, info = %s", cs.getClass(), info));
            });
        } catch (NoImplementClassException e) {
            DefaultCodeScanner defaultCodeScanner = new DefaultCodeScanner();
            String info = defaultCodeScanner.scan();
            System.out.println(String.format("[CodeScanner]: %s, info = %s", defaultCodeScanner.getClass(), info));
        }
    }
    
}
