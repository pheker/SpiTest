package cn.pheker.codescanner;

import cn.pheker.spi.ClassUtil;
import cn.pheker.spi.NoImplementClassException;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author cn.pheker
 * @date   2019/2/24 17:43
 * @email  1176479642@qq.com
 * @desc   具体的产品
 */
public class TheKingOfCodeScanner {
    
    public void process() throws NoImplementClassException {
        //1.扫描
        ClassUtil.invokeSpi(CodeScanner.class,codeScanner -> {
            String info = codeScanner.scan();
            if (info == null || info.length() == 0) {
                System.out.println("扫描失败！请重试");
            }else{
                //2.解析信息
                    //3.普通信息，显示
                    //4.是网址-跳转并访问
                    //5.可以是定义好的特殊功能信息，如登陆、身份识别、密钥等
                if (info.contains("http")) {
                    String content = readWebsite(info);
                    if(content != null && content.length() > 0) {
                        System.out.println("content = " + content);
                    }
                }else{
                    //and so on
    
                    System.out.println("info = " + info);
                }
            }
        });
    }
    
    /**
     * 获取网址信息
     * @param info
     * @return
     */
    private String readWebsite(String info) {
        String website = info;
        Pattern pattern = Pattern.compile("https?://[^:/\\s]+(?::\\d+)?(?:(?:/[^:/\\s]*)+(?:[\\w_.]+(?:\\.\\w+)?)?)?");
        Matcher matcher = pattern.matcher(website);
        if (matcher.find()) {
            website = matcher.group();
            System.out.println("[正在访问网址]: "+website);
        }
        String[] split = website.split("[:/]+");
        int port = 80;
        if(split.length > 2) {
            String sPort = split[2];
            if (sPort != null && sPort.length() != 0) {
                port = Integer.valueOf(sPort);
            }
        }
        String file = website.replace("https?://[^:/]+(?::\\d+)?/?", "");
        InputStream is = null;
        try {
            URL url = new URL(split[0], split[1], port, file);
            URLConnection conn = url.openConnection();
            conn.setReadTimeout(1000);
            is = (InputStream) conn.getContent();
            byte[] buf = new byte[is.available()];
            is.read(buf);
            return new String(buf);
        } catch (Exception e) {
            System.out.println("[网址访问失败]: "+ website);
        }finally {
            if(is != null){
                try {
                    is.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return null;
    }
    
    
}
