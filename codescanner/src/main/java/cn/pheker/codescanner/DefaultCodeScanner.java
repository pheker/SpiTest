package cn.pheker.codescanner;

/**
 * @author cn.pheker
 * @date 2019/2/23 20:49
 * @email 1176479642@qq.com
 * @desc
 */
public class DefaultCodeScanner implements CodeScanner{
    @Override
    public String scan() {
        return "You are using DefaultCodeScanner now.";
    }
}
