package cn.pheker.codescanner;


import cn.pheker.spi.Spi;

/**
 * @author cn.pheker
 * @date   2019/2/22 23:08
 * @email  1176479642@qq.com
 * @desc   图形码扫描器接口spi
 */

@Spi
public interface CodeScanner {
    
    /**
     * 扫描图形码并读取信息
     * @return
     */
    String scan();
    
}
