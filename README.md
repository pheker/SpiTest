# SpiTest

#### 介绍
测试SPI

#### 软件架构
SpiTest  
|  
|__spi  
|  
|__codescanner  
|　　|  
|　　|__barcodescanner  
|　　|  
|　　|__qrcodescanner  
|          
|__testcodescanner  


1.spi模块
 * 定义了Spi注解
 * 用于表明某个接口是个扩展接口
 * 其它人可以根据此接口定义具体的实现
 * 提供了查找扩展接口的工具类ClassUtil
 
2.codescanner
 * 定义了图形码扫描器规范(接口)
 * 并提供了默认的扫描器实现
 * 利用SPI机制可以使用具体厂商的实现，以做后续操作，如解读获取的信息，识别网址，再实现其它的功能
 
3.barcodescanner
 * 条形码扫描器-是codescanner规范的个体实现
 * 用于扫描条形码

4.qrcodescanner
 * 二维码扫描器-是codescanner规范的个体实现
 * 用于扫描二维码
 
5.testcodescanner
即codescanner规范测试功能，测试SPI是否可以使用
 


#### 安装教程

1. xxxx
2. xxxx
3. xxxx

#### 使用说明

1. xxxx
2. xxxx
3. xxxx

#### 参与贡献

1. Fork 本仓库
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request


#### 码云特技

1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2. 码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4. [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5. 码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6. 码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)