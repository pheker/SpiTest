package cn.pheker.qrcodescanner;

import cn.pheker.codescanner.CodeScanner;

/**
 * @author cn.pheker
 * @date   2019/2/22 23:32
 * @email  1176479642@qq.com
 * @desc   二维码扫描器，用于扫描二维码并获取其包含的信息
 */
public class QrCodeScanner implements CodeScanner {
    
    @Override
    public String scan() {
    
        System.out.println("QrCodeScanner is scanning QrCode information...");
        
        return "http://www.baidu.com";
    }
}
