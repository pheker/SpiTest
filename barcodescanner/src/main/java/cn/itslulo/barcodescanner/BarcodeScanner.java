package cn.itslulo.barcodescanner;

import cn.pheker.codescanner.CodeScanner;

/**
 * @author cn.pheker
 * @date   2019/2/24 16:56
 * @email  1176479642@qq.com
 * @desc   条形码扫描器
 */
public class BarcodeScanner implements CodeScanner {
    @Override
    public String scan() {
    
        System.out.println("BarcodeScanner is scanning BarCode information...");
    
        return "BarcodeScanner-http://www.itslulo.cn";
    }
}
