package cn.pheker.testcodescanner;


import cn.pheker.codescanner.CodeScanner;
import cn.pheker.codescanner.CodeScannerUtil;
import cn.pheker.codescanner.TheKingOfCodeScanner;
import cn.pheker.spi.ClassUtil;
import cn.pheker.spi.NoImplementClassException;
import cn.pheker.spi.Spi;
import org.junit.Test;

import java.util.Iterator;
import java.util.ServiceLoader;
import java.util.Set;

/**
 * @author cn.pheker
 * @date   2019/2/22 23:36
 * @email  1176479642@qq.com
 * @desc   测试不同的图形码扫描器
 */
public class TestCodeScanner {
    
    @Test
    public void testSpi(){
        Set<Class<?>> interfaces = ClassUtil.getTypesAnnotatedWith(Spi.class);
        System.out.println("interfaces = " + interfaces);
    }
    
    @Test
    public void testCodeScannerWithAnnotationSpi() {
        TheKingOfCodeScanner king = new TheKingOfCodeScanner();
        try {
            king.process();
        } catch (NoImplementClassException e) {
            e.printStackTrace();
        }
    }
    
    @Test
    public void testSereviceLoader(){
        ServiceLoader<CodeScanner> loader = ServiceLoader.load(CodeScanner.class);
        Iterator<CodeScanner> codeScannerIte = loader.iterator();
        while(codeScannerIte.hasNext()) {
            CodeScanner cs = codeScannerIte.next();
            String info = cs.scan();
            System.out.println("info = " + info);
        }
    }
    
    
}
